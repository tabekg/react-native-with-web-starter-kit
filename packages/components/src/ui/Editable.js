import React, {Component} from 'react';
import {inject, observer} from "mobx-react";
import {Input, CheckBox} from "react-native-elements";

export function createEditable(WrappedComponent, valuePropertyName = 'value', changePropertyName = 'onChange', changeOverride = null) {
  return inject('rootStore')(observer(class extends Component {
    constructor(props) {
      super(props);
    }

    _onChange = (value) => {
      let {model, name, converter} = this.props;
      if (converter) {
        model.set(name, converter.to(value));
      } else {
        model.setValue(name, value);
      }
    };

    render() {
      let {model, name, converter, ...rest} = this.props;
      if (model) {
        if (converter) {
          rest[valuePropertyName] = converter.from(model[name]);
        } else {
          rest[valuePropertyName] = model[name];
        }
        rest[changePropertyName] = this._onChange;
        if (changeOverride)
          rest[changePropertyName] = () => changeOverride(this.props, this._onChange);
      }
      return <WrappedComponent {...rest}/>
    }
  }))
}

export const AppInput = createEditable(Input, 'value', 'onChangeText');

export const AppCheckBox = createEditable(CheckBox, 'checked', 'onPress', (props, fn) => {
  let {model, name} = props;
  fn(!model[name])
});
