import React from 'react';
import PropTypes from 'prop-types';
import {observer, inject} from "mobx-react";
import {Picker} from "react-native";
import rootStore from "../stores/RootStore";

@inject('rootStore') @observer
export default class AppSelect extends React.Component {
  static propTypes = {
    model: PropTypes.object,
    name: PropTypes.string,
    options: PropTypes.array,
    dict: PropTypes.string,
    onChange: PropTypes.func,
    labelKey: PropTypes.string,
    valueKey: PropTypes.string,
    prompt: PropTypes.string,
    mode: PropTypes.oneOf(['dropdown', 'dialog']),
    readOnly: PropTypes.bool,
  };

  static defaultProps = {
    labelKey: 'name',
    valueKey: 'id',
    readOnly: false,
    mode: 'dropdown',
  };

  constructor(props, context) {
    super(props, context);
    this.state = {options: []};
  }

  onChange = (selected, i) => {
    let {model, name, onChange} = this.props;
    if (model && name) {
      model.setValue(name, parseInt(selected));
    }
    if (onChange) {
      onChange(selected);
    }
  };

  componentDidMount() {
    this.refreshOptions();
  }

  refreshOptions() {
    let {options, dict} = this.props;
    if (options) this.setOptions(options);
    if (dict) {
      rootStore.dictStore.getDictionary(dict).then((options) => this.setOptions(options))
    }
  }

  setOptions(options) {
    this.setState({options: [{id: null, name: '(Unselected)'}, ...options]});
  }

  componentWillReact() {
    this.refreshOptions();
  }

  render() {
    let {model, name, options, labelKey, valueKey, mode, prompt, readOnly, selected, dict, ...props} = this.props;
    let item = selected;
    if (model && name) {
      item = model[name];
    }
    return (<Picker {...props} selectedValue={item} onValueChange={this.onChange}
                    mode={mode} prompt={prompt}>
      {this.state.options.map(this.mapOptions)}
    </Picker>)
  }

  mapOptions = (o) => {
    return (<Picker.Item key={o[this.props.valueKey]} label={o[this.props.labelKey]}
                         value={o[this.props.valueKey]}/>);
  }
}
