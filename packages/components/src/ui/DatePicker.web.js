import React from 'react';
import {AppInput} from "./Editable";
import moment from 'moment';

export default class DatePicker extends React.Component {
  input;

  constructor(props, context) {
    super(props, context);
    this.state = {show: false};
  }

  _to = (value) => {
    return moment(value).toDate();
  };

  _from = (value) => {
    return moment(value).format('DD/MM/YYYY')
  };

  _toggleModal = () => {
    this.setState({show: !this.state.show});
    if (this.input && this.input.blur)
      this.input.blur();
  };

  render() {
    let {...rest} = this.props;
    return [
      <AppInput ref={(r) => this.input = r} converter={{to: this._to, from: this._from}}
                rightIcon={{name: 'ios-calendar', type: 'ionicon'}}
                onFocus={this._toggleModal}
                {...rest}/>,

    ]
  }
}
