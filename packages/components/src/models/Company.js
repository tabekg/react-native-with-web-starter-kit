import {types} from 'mobx-state-tree';
import {User} from './User';

export const Company = types.model('Company', {
  id: types.identifier,
  name: types.string,
  owner: types.reference(User)
});
