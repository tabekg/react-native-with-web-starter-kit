import React from 'react';
import {types} from 'mobx-state-tree';
import {AppInput} from "../ui/Editable";
import {uuid} from "../utils";
import AppSelect from "../ui/AppSelect";

export function editableModel() {
  return types.model({})
    .actions(self => ({
      setValue: (k, value) => {
        self[k] = value;
      }
    }))
    .views(self => ({
      editor: function () {
        let items = [];
        Object.keys(self).forEach((k) => {
          let p = self.$treenode.type.properties[k];
          if (p.name.indexOf('identifier') >= 0) return;
          if (p._types[0].name === 'string')
            items.push(<AppInput key={k} placeholder={k} model={self} name={k}/>);
          if (p._types[0].name === 'dict') {
            let d = p._types[0].create({});
            items.push(<AppSelect model={self} name={k} dict={d.table}/>)
          }
          console.log(p._types);
        });
        return items;
      }
    }));
}

export function withLocalIdentity() {
  return types.model({local_id: types.optional(types.identifier, uuid())})
}

function dict(dictionary) {
  return types.model('dict', {table: types.optional(types.string, dictionary)})
}

export const custom_types = {
  dict,
};

