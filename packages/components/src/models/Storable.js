import {applySnapshot, onSnapshot, types} from "mobx-state-tree";
import requester from "../requester";

export function createStorable(endpoint) {
  return types.model({})
    .actions(self => ({
        async sendToApi(item) {
          let data = await requester.post(endpoint, item);
          if (data) {
            data.item.local_id = self.local_id;
            if (self.id !== data.item.id)
              applySnapshot(self, data.item);
          }
        },
        afterCreate() {
          onSnapshot(self, (snapshot => {
            console.log(endpoint, snapshot);
            self.sendToApi(snapshot);
          }));
        }
      }
    ))
}

