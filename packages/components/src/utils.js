import {observer} from "mobx-react"

export function components(fns) {
  Object.keys(fns).forEach(key => {
    fns[key] = observer(fns[key])
  });
  return fns
}

export function uuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + '-' + s4() + '-' + s4() + '-' + s4();
}
