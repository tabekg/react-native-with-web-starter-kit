import React from 'react';
import {Text, AsyncStorage, Button} from 'react-native';
import {ScreenWrapper} from "./Components";

export default class SettingsScreen extends React.Component {
  static navigationOptions = {
    title: 'Settings',
  };

  render() {
    return (
      <ScreenWrapper>
        <Text>Settings!</Text>
        <Button title="Sign me out" onPress={this._signOutAsync}/>
      </ScreenWrapper>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
}
