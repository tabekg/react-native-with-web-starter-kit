export const DEV_URL = 'http://localhost:5000';
export const TEST_URL = 'https://';
export const PROD_URL = 'https://';

console.log('env', process.env.APP_ENV, process.env.NODE_ENV);
export const appStatePersistenceKey = 'appStatePersistenceKey';

/**
 * @return {string}
 */
export function WEB_URL() {
  if (process.env.APP_ENV === 'development')
    return DEV_URL;
  if (process.env.APP_ENV === 'test')
    return TEST_URL;
  if (process.env.APP_ENV === 'production')
    return PROD_URL;
  return process.env.NODE_ENV === "development" ? DEV_URL : PROD_URL;
}

/**
 * @return {string}
 */
export function API_URL() {
  return `${WEB_URL()}/api/v1`;
}
