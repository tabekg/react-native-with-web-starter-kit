import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {createBottomTabNavigator} from 'react-navigation-tabs';
import HomeScreen from "../screens/HomeScreen";
import SettingsScreen from "../screens/SettingsScreen";

const HomeStack = createStackNavigator({
  HomeScreen,
});

HomeStack.navigationOptions = {
  tabBarLabel: 'Home',
};

const SettingsStack = createStackNavigator({
  Settings: SettingsScreen,
});

SettingsStack.navigationOptions = {
  tabBarLabel: 'Settings'
};

export const createMainTabNavigator = rootStore => {
  const TargetComponent = props => {
    return <HomeStack {...props} screenProps={rootStore}/>;
  };

  return createBottomTabNavigator({
    HomeStack,
    SettingsStack,
  });
};
