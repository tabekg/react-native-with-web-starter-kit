/**
 * Created by jaynakus on 7/2/17.
 */
import axios from "axios";
import {API_URL} from "./settings";
import rootStore from "./stores/RootStore";

function token() {
  let {userStore} = rootStore;
  if (userStore.current) {
    return userStore.current.token;
  }
  return '';
}

function request(method, url, data, params, silent = false) {
  return axios.request({method, url: `${API_URL()}${url}`, data, params, headers: {'authorization': token()}})
    .then(({data}) => {
      console.log(data);
      if (data.result === 0) {
        return data;
      }
      if (data.result === 1) {
        let {userStore} = rootStore;
        userStore.signOut();
      } else if (!silent) {
        alert(data.message);
      }
      return null;
    })
    .catch((error) => {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message);
      }
      console.log(error.config);
      alert('Something went wrong');
      return null;
    })
}

const requester = {
  get: function (url, params = null, silent = false) {
    return request('get', url, null, params, silent);
  },
  post: function (url, data = null, params = null, silent = false) {
    return request('post', url, data, params, silent);
  },
  put: function (url, data = null, params = null, silent = false) {
    return request('put', url, data, params, silent);
  },
  delete: function (url, params = null, silent = false) {
    return request('delete', url, null, params, silent);
  },
};

export default requester;
